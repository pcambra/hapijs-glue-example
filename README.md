# README

Using glue to compose a hapi server.

# Installation

Prerequisites: Node >= 6.9.x

```bash
npm install
```

Optional: nodemon

```bash
npm install -g nodemon
```

## Config File

We are using `getconfig` module. All configurations are stored inside `config/` directory. Create `config/local.json` file:

```
{
  "redis": {
    "host": "127.0.0.1",
    "port": 6379,
    "password": "",
    "database": 1
  },
  "cookie": {
    "password": "32-characters-long"
  },
  "facebook": {
    "password": "32-characters-long",
    "clientId": "",
    "clientSecret": ""
  }
}
```

# Start server

```bash
node .

# or nodemon
nodemon .
```

# License

* MIT
