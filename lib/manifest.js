const config = require('getconfig');

module.exports = {
  server: {
    cache: [
      {
        name: 'redisCache',
        engine: 'catbox-redis',
        host: config.redis.host,
        port: config.redis.port,
        password: config.redis.password,
        database: config.redis.database,
        partition: 'cache'
      }
    ]
  },
  connections: [
    {
      labels: ['api'],
      host: config.host,
      port: config.port
    },
    {
      labels: ['admin'],
      host: config.host,
      port: 9005
    }
  ],
  registrations: [
    { plugin: 'inert' },
    { plugin: 'vision' },
    { plugin: 'lout' },
    { plugin: 'bassmaster' },
    { plugin: 'hapi-auth-cookie' },
    { plugin: 'bell' },
    {
      plugin: './routes/auth',
      options: {
        select: ['admin']
      }
    },
    { plugin: './routes/home' },
    { plugin: './routes/cache' },
    {
      plugin: './routes/upload',
      options: {
        select: ['admin']
      }
    },
    {
      plugin: {
        register: 'good',
        options: {
          reporters: {
            myConsoleReport: [
              {
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{ log: '*', response: '*' }]
              },
              { module: 'good-console' },
              'stdout'
            ]
          }
        }
      }
    },
    {
      plugin: './routes/prefixed-routes',
      options: {
        routes: {
          prefix: '/v2'
        },
        select: ['api']
      }
    },
    {
      plugin: './routes/validate',
      options: {
        routes: {
          prefix: '/validate'
        }
      }
    },
    { plugin: './routes/static' },
    { plugin: './routes/404' }
  ]
};
