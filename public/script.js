var Nes = require('nes/client');

var mySocket = new Nes.Client('ws://localhost:9000');
var jsDom = document.querySelector('#js');
var sendMsg = document.querySelector('#nMessage');
var timespan = document.querySelector('#time');
var item5 = document.querySelector('#item5');
var printResult = document.querySelector('#printResult');
var getSubs = document.querySelector('#getSubscriptions');
var getClientId = document.querySelector('#getClientId');
var unsubBtn = document.querySelector('#unsub');
var nRequest = document.querySelector('#nRequest');
var path = document.querySelector('#path');

if (jsDom) {
  mySocket.connect(err => {
    if (err) {
      console.error(err);
    }

    mySocket.subscribe(
      '/item/5',
      (update, flags) => {
        item5.innerHTML = JSON.stringify(update, null, 4);
        console.log(
          'subscribe "/item/5", data: %s, flags: %s',
          JSON.stringify(update),
          JSON.stringify(flags)
        );
      },
      err => {
        if (err) {
          console.error(err);
        }
      }
    );

    mySocket.subscribe(
      '/time',
      (update, flags) => {
        timespan.innerHTML = Date(update.message);
        console.log(
          'subscribe "/time", data: %s, flags: %s',
          JSON.stringify(update),
          JSON.stringify(flags)
        );
      },
      err => {
        if (err) {
          console.error(JSON.stringify(err));
        }
      }
    );
  });

  mySocket.onConnect = () => {
    console.log('Nes.onConnection: connection established');

    sendMsg.addEventListener('click', sendMsgFuntion, false);
    getSubs.addEventListener('click', getSubsFunction, false);
    getClientId.addEventListener('click', getClientIdFunc, false);
    unsubBtn.addEventListener('click', unSubscribe, false);
    nRequest.addEventListener('click', requestFunc, false);
  };

  mySocket.onError = err => {
    console.log('Nes.onError: %s', JSON.stringify(err));

    sendMsg.removeEventListener('click', sendMsgFuntion, false);
    getSubs.removeEventListener('click', getSubsFunction, false);
    getClientId.removeEventListener('click', getClientIdFunc, false);
    unsubBtn.removeEventListener('click', unSubscribe, false);
    nRequest.removeEventListener('click', requestFunc, false);
  };

  mySocket.onUpdate = update => {
    console.log(
      'Server -> Client\nNes.onUpdate: \n%s',
      JSON.stringify(update, null, 2)
    );
  };
}
// send a message to server
function sendMsgFuntion() {
  var data = {
    id: 'hello',
    msg: path.value
  };
  mySocket.message(data, (err, msg) => {
    if (err) {
      return console.log(err);
    }
    console.log('Nes.message(): %s', JSON.stringify(msg));
    printResult.textContent = JSON.stringify(msg, null, 2);
  });
}
// list current subscriptions
function getSubsFunction() {
  printResult.textContent = JSON.stringify(mySocket.subscriptions());
}
// get client's socket id
function getClientIdFunc() {
  printResult.textContent = mySocket.id;
}
// unsubscribe a channel
function unSubscribe() {
  mySocket.unsubscribe('/item/5', null, err => {
    if (err) {
      return console.log(err);
    }
  });
}
// ajax, but using websocket
function requestFunc() {
  mySocket.request(
    {
      path: path.value,
      method: 'GET' // POST, PUT, DELETE
    },
    requestHandler
  );
}
function requestHandler(err, payload, statusCode, headers) {
  if (err) {
    return console.log(JSON.stringify(err));
  }
  console.log(payload, statusCode, headers);
  if (typeof payload === 'object') {
    printResult.textContent = JSON.stringify(payload, null, 2);
    return;
  }
  printResult.textContent = payload;
}
