const Glue = require('glue');
const Hoek = require('hoek');

const manifest = require('./lib/manifest');

const options = {
  relativeTo: __dirname
};

if (process.env.NODE_ENV !== 'production') {
  manifest.registrations.push({
    plugin: {
      register: 'blipp',
      options: {
        showAuth: true
      }
    }
  });
}

Glue.compose(manifest, options, (err, server) => {
  Hoek.assert(!err, err);

  server.views({
    defaultExtension: 'hbs',
    isCached: false,
    engines: {
      hbs: require('handlebars')
    },
    relativeTo: __dirname,
    layout: 'default',
    path: './templates',
    helpersPath: './templates/helpers',
    layoutPath: './templates/layout',
    partialsPath: './templates/partials'
  });

  // render error page
  server.ext('onPreResponse', (request, reply) => {
    if (!request.response.isBoom) {
      return reply.continue();
    }
    return reply
      .view('better404', request.response)
      .code(request.response.output.statusCode);
  });

  server.start(err => {
    Hoek.assert(!err, err);
    server.connections.map(s => {
      console.log(`App "${s.settings.labels}" is running at: ${s.info.uri}`);
    });
  });
});
