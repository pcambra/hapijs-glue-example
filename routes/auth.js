const Config = require('getconfig');
const Hoek = require('hoek');
const isSecure = process.env.NODE_ENV === 'production';

exports.register = (server, options, next) => {
  /**
   * Catbox configuration
   */
  const cache = server.cache({
    cache: 'redisCache', // catbox cache name, see server.cache in `lib/manifest.js`
    segment: 'sessions',
    expiresIn: 7 * 24 * 60 * 60 * 1000 // 7 days
  });
  server.app.cache = cache;

  /**
   * Authentication Strategy
   */
  server.auth.strategy('session', 'cookie', {
    password: Config.cookie.password,
    redirectTo: '/bell/facebook',
    isSecure: isSecure,
    validateFunc: function(request, session, callback) {
      cache.get(session.sid, (err, cached) => {
        if (err) {
          return callback(err, false);
        }
        if (!cached) {
          return callback(null, false);
        }
        return callback(null, true, cached.account);
      });
    }
  });
  /**
   * Visit https://developers.facebook.com/apps/ and create a new app.
   * Add Product, choose Facebook Login,
   * Valid OAuth redirect URIs: http://localhost:9000/bell/facebook
   */
  server.auth.strategy('facebook', 'bell', {
    provider: 'facebook',
    isSecure: isSecure,
    password: Config.facebook.password,
    clientId: Config.facebook.clientId, // App ID
    clientSecret: Config.facebook.clientSecret // App Secret
  });

  server.route({
    method: 'GET',
    path: '/bell',
    config: {
      auth: {
        strategy: 'session',
        mode: 'try'
      },
      handler: (request, reply) => {
        const isAuthenticated = request.auth.isAuthenticated;
        reply.view('login', {
          isAuthenticated,
          user: request.auth.credentials
        });
      },
      plugins: {
        'hapi-auth-cookie': { redirectTo: false }
      }
    }
  });
  server.route({
    method: 'GET',
    path: '/bell/logout',
    config: {
      auth: 'session',
      handler: (request, reply) => {
        // read cookie
        const sid = request.state.sid.sid;
        // drop session in catbox
        request.server.app.cache.drop(sid, err => {
          Hoek.assert(!err, err);
          // clear session cookie
          request.cookieAuth.clear();
          return reply.redirect('/bell');
        });
      }
    }
  });
  server.route({
    method: 'GET',
    path: '/bell/facebook',
    config: {
      auth: 'facebook',
      handler: (request, reply) => {
        // findOrInsert user
        const profile = request.auth.credentials.profile;
        // set session id
        const sid = `facebook:${profile.id}`;
        // set session
        request.server.app.cache.set(
          sid,
          {
            account: profile
          },
          0,
          err => {
            Hoek.assert(!err, err);
            request.cookieAuth.set({ sid: sid });
            return reply.redirect('/bell');
          }
        );
      }
    }
  });

  next();
};

exports.register.attributes = {
  name: 'my auth',
  version: '1.0.0'
};
