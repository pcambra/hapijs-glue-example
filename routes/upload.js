const path = require('path');
const fs = require('fs');
const Boom = require('boom');
const cryptiles = require('cryptiles');
const Hoek = require('hoek');
const imageType = require('image-type');
const mkdirp = require('mkdirp');
const pSettle = require('p-settle');

exports.register = (server, opt, next) => {
  server.route([
    {
      method: 'GET',
      path: '/upload',
      handler: (request, reply) => {
        reply.view('upload');
      }
    },
    {
      method: 'POST',
      path: '/upload',
      config: {
        payload: {
          output: 'stream',
          allow: 'multipart/form-data',
          parse: true,
          maxBytes: 1048576, // 1MB
          timeout: 5 * 1000 // 5s
        },
        description: 'Upload files',
        tags: ['Image'],
        handler: (request, reply) => {
          const CURRENT_YEAR = new Date().getFullYear();
          const CURRENT_MONTH = new Date().getMonth() + 1;
          const UPLOAD_PATH = `public/uploads/${CURRENT_YEAR}/${CURRENT_MONTH}`;

          mkdirp(path.join(__dirname, '..', UPLOAD_PATH), err => {
            Hoek.assert(!err, err);

            const upload = uploader(request.payload.file, {
              path: UPLOAD_PATH,
              year: CURRENT_YEAR,
              month: CURRENT_MONTH
            });

            upload
              .then(result => {
                reply.view('upload', {
                  success: true,
                  raw: result
                });
              })
              .catch(err => {
                const statusCode = err.isBoom ? err.output.statusCode : 400;
                const errorMsg = err.isBoom
                  ? err.message
                  : 'Something went wrong';
                return reply
                  .view('upload', {
                    success: false,
                    error: true,
                    errorMsg
                  })
                  .code(statusCode);
              });
          });
        }
      }
    }
  ]);

  next();
};

function uploader(file, options) {
  return Array.isArray(file)
    ? _filesHandler(file, options)
    : _fileHandler(file, options);
}
function _filesHandler(files, opt) {
  const writeFiles = files.map(x => _fileHandler(x, opt));
  return pSettle(writeFiles);
}
function _fileHandler(file, opt) {
  // only accept image (non-svg)
  const pic = imageType(file._data);
  if (!pic) {
    return Promise.reject(Boom.unsupportedMediaType());
  }

  const RANDOM_FILE_NAME = cryptiles.randomString(12);
  const FILE_EXT = pic.ext;
  const newFileName = `${RANDOM_FILE_NAME}.${FILE_EXT}`;
  const fileStream = fs.createWriteStream(
    path.join(__dirname, '..', opt.path, newFileName)
  );

  return new Promise((resolve, reject) => {
    file.on('error', err => reject(err));

    file.pipe(fileStream);

    file.on('end', err => {
      Hoek.assert(!err, err);

      const metaData = {
        publicPath: `/assets/uploads/${opt.year}/${opt.month}/${newFileName}`,
        internal: {
          filePath: `${opt.path}/${newFileName}`,
          mimeType: pic
        }
      };
      resolve(metaData);
    });
  });
}

exports.register.attributes = {
  name: 'upload route',
  version: '1.0.2'
};
