const faker = require('faker');
const Joi = require('joi');

exports.register = (server, options, next) => {
  function onMessage(socket, data, next) {
    console.log(data);
    const echo = {
      type: 'broadcast',
      sender: socket.id,
      data
    };
    server.broadcast(echo);
    next('[Server] Nes.onMessage handler. Reply interface for Nes websocket.');
  }
  // register nes
  server.register({
    register: require('nes'),
    options: {
      onMessage
    }
  });
  // nes
  server.subscription('/item/{id}', {
    onSubscribe: (socket, path, params, next) => {
      console.log('[NES] Server.onSubscribe');
      console.log(
        `path: ${JSON.stringify(path)}, params: ${JSON.stringify(params)}`
      );
      next();
    },
    onUnsubscribe: (socket, path, params, next) => {
      console.log('[NES] Server.onUnsubscribe');
      console.log(
        `path: ${JSON.stringify(path)}, params: ${JSON.stringify(params)}`
      );
      next();
    }
  });
  server.subscription('/time');

  server.route([
    {
      method: 'GET',
      path: '/api/ping',
      handler: (request, reply) => {
        reply({
          status: 'ok'
        });
      }
    },
    {
      method: 'GET',
      path: '/api/user/{id}',
      config: {
        validate: {
          params: {
            id: Joi.string().alphanum().max(50).required()
          }
        },
        tags: ['api', 'fake'],
        handler: (request, reply) => {
          reply({
            userId: String(request.params.id),
            displayName: faker.name.findName(),
            email: faker.internet.email(),
            avatar: faker.internet.avatar()
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/realtime',
      config: {
        id: 'hello',
        handler: (request, reply) => {
          request.socket.publish('/time', {
            id: 90,
            message: new Date().getTime()
          });
          request.socket.publish('/item/5', {
            id: 555,
            message: 'this is item #5'
          });
          request.server.broadcast('welcome!');

          return reply({
            id: 'hello',
            path: request.path
          });
        }
      }
    }
  ]);

  return next();
};

exports.register.attributes = {
  name: 'prefixed API routes',
  version: '1.0.0'
};
