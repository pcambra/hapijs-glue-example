const Joi = require('joi');

exports.register = (server, options, next) => {
  server.route([
    {
      method: 'GET',
      path: '/cool/{uid}',
      config: {
        validate: {
          params: {
            uid: Joi.string().guid()
          }
        },
        handler: (request, reply) => {
          reply.view('validate', {
            data: request.params.uid
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/output',
      config: {
        handler: (request, reply) => {
          reply({
            status: 200,
            message: 'output is validated',
            more: 'moar!'
          });
        },
        response: {
          failAction: 'error', // "error" default value, will return 500 instead
          schema: {
            status: Joi.number().valid([200]),
            message: Joi.string().max(100)
          }
        }
      }
    }
  ]);

  return next();
};

exports.register.attributes = {
  name: 'validate route',
  version: '1.0.0'
};
