exports.register = (server, options, next) => {
  server.route([
    {
      method: 'GET',
      path: '/{p*}',
      handler: (request, reply) => {
        reply.view('404', null, { layout: false }).code(404);
      }
    }
  ]);

  next();
};

exports.register.attributes = {
  name: 'bad route',
  version: '1.0.0'
};
